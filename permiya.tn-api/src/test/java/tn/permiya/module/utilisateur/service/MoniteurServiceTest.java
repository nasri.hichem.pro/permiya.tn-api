package tn.permiya.module.utilisateur.service;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import tn.permiya.module.adresse.entite.Adresse;
import tn.permiya.module.ecole.entity.Etablissement;
import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.repository.RoleRepository;

/**
 * 
 * @author Permiya.tn Classe de test UtilisateurService pour Utilisateur de type
 *         moniteur
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:applicationtest.properties")
public class MoniteurServiceTest {

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private RoleRepository roleRepository;

	private Moniteur moniteur;

	private Etablissement etablissement;


	@Before
	public void setUp() {

		NomRole nomRole = NomRole.ROLE_MONITEUR;
		Role roleUtilisateur = roleRepository.getRoleByNomRole(NomRole.ROLE_MONITEUR);
		if (roleUtilisateur == null) {
			roleUtilisateur = new Role(nomRole);
			roleUtilisateur = roleRepository.save(roleUtilisateur);
		}

		Set<Role> roles = new HashSet<Role>();
		roles.add(roleUtilisateur);

		Adresse adresseEtablissement = new Adresse("Tunis", "Beb Bhar", "31 rue Ibn Khouldoun", 2000, null, null);
		etablissement = new Etablissement("Auto ecole test", adresseEtablissement);
		moniteur = new Moniteur("testNomMoniteur", "testMoniteurPrenom", "testMoniteur@gmail.com", "testMoniteur123",
				"0021671000000", "0021699999999", null, roles, etablissement);

	}

	@Test
	public void testAjouterMoniteur() {

		Moniteur moniteurAjoute = (Moniteur) utilisateurService.ajouterOuModifierUtilisateur(moniteur);
		Optional<Utilisateur> expectedMoniteur = utilisateurService
				.getUtilisateurById(moniteurAjoute.getIdUtilisateur());

		assertEquals(expectedMoniteur.get().getIdUtilisateur(), moniteurAjoute.getIdUtilisateur());
		assertEquals(expectedMoniteur.get().getNom(), moniteurAjoute.getNom());
		assertEquals(expectedMoniteur.get().getPrenom(), moniteurAjoute.getPrenom());
		assertEquals(expectedMoniteur.get().getEmail(), moniteurAjoute.getEmail());
		assertEquals(expectedMoniteur.get().getMotPasse(), moniteurAjoute.getMotPasse());
		assertEquals(expectedMoniteur.get().getNumeroTelephone(), moniteurAjoute.getNumeroTelephone());
		assertEquals(((Moniteur) expectedMoniteur.get()).getEtablissement().getAdresse().getVille(),
				moniteurAjoute.getEtablissement().getAdresse().getVille());
		assertEquals(((Moniteur) expectedMoniteur.get()).getEtablissement().getAdresse().getRue(),
				moniteurAjoute.getEtablissement().getAdresse().getRue());
		assertEquals(((Moniteur) expectedMoniteur.get()).getEtablissement().getAdresse().getCodePostale(),
				moniteurAjoute.getEtablissement().getAdresse().getCodePostale());
	}

}
