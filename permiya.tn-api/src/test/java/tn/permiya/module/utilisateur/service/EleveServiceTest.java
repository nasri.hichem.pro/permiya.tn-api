package tn.permiya.module.utilisateur.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import tn.permiya.module.adresse.entite.Adresse;
import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.repository.RoleRepository;

/**
 * 
 * @author Permiya.tn Classe de test UtilisateurService pour Utilisateur de type
 *         eleve
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:applicationtest.properties")
public class EleveServiceTest {

	@Autowired
	private UtilisateurService utilisateurService;

	@Autowired
	private EleveService eleveService;

	@Autowired
	private RoleRepository roleRepository;

	private Eleve eleve;

	private Adresse adresse;


	@Before
	public void setUp() {

		
		Role roleUtilisateur=roleRepository.getRoleByNomRole(NomRole.ROLE_ELEVE);
		if(roleUtilisateur==null) {
			roleUtilisateur=new Role(NomRole.ROLE_ELEVE);
			roleUtilisateur=roleRepository.save(roleUtilisateur);
		}
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleUtilisateur);
		eleve = new Eleve("eleveNomTest", "elevePrenomTest", "eleveTest@gmail.com", "eleve123", "0021671000000",
				"0021699999999", null, roles, new Date());
		
		adresse = new Adresse("Ben arous", "Mourouj", "39 rue islande", 2075, null, null);
		eleve.setAdresse(adresse);

	}

	@Test
	public void testAjouterEleve() {
		Eleve eleveAjoute = (Eleve) utilisateurService.ajouterOuModifierUtilisateur(eleve);
		Optional<Utilisateur> expectedEleve = utilisateurService.getUtilisateurById(eleve.getIdUtilisateur());

		assertEquals(expectedEleve.get().getIdUtilisateur(), eleveAjoute.getIdUtilisateur());
		assertEquals(expectedEleve.get().getNom(), eleveAjoute.getNom());
		assertEquals(expectedEleve.get().getPrenom(), eleveAjoute.getPrenom());
		assertEquals(expectedEleve.get().getEmail(), eleveAjoute.getEmail());
		assertEquals(expectedEleve.get().getMotPasse(), eleveAjoute.getMotPasse());
		assertEquals(expectedEleve.get().getNumeroTelephone(), eleveAjoute.getNumeroTelephone());
		assertEquals(((Eleve) expectedEleve.get()).getAdresse().getVille(), eleveAjoute.getAdresse().getVille());
		assertEquals(((Eleve) expectedEleve.get()).getAdresse().getRue(), eleveAjoute.getAdresse().getRue());
		assertEquals(((Eleve) expectedEleve.get()).getAdresse().getCodePostale(),
				eleveAjoute.getAdresse().getCodePostale());
	}

	@Test
	public void testgetListEleves() {
		utilisateurService.ajouterOuModifierUtilisateur(eleve);
		List<Eleve> listEleves = eleveService.getListeEleves();
		assertNotEquals(0, listEleves.size());

	}

}
