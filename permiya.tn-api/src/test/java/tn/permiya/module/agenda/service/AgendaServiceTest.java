package tn.permiya.module.agenda.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import tn.permiya.module.adresse.entite.Adresse;
import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.agenda.entite.RendezVous;
import tn.permiya.module.ecole.entity.Etablissement;
import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;
import tn.permiya.module.utilisateur.repository.RoleRepository;
import tn.permiya.module.utilisateur.service.MoniteurService;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:applicationtest.properties")
public class AgendaServiceTest {

	@Autowired
	private AgendaService agendaService;

	@Autowired
	private MoniteurService moniteurService;

	@Autowired
	private RoleRepository roleRepository;

	private Agenda agenda;

	@Before
	public void setUp() {

		creerAgenda();
	}

	private void creerAgenda() {
		this.agenda = new Agenda();
		List<RendezVous> listRdv = new ArrayList<RendezVous>();
		agenda.setListRendezVous(listRdv);

		NomRole nomRole = NomRole.ROLE_MONITEUR;
		Role roleUtilisateur=roleRepository.getRoleByNomRole(nomRole);
		if(roleUtilisateur==null) {
			roleUtilisateur=new Role(nomRole);
			roleUtilisateur=roleRepository.save(roleUtilisateur);
		}
		Set<Role> roles = new HashSet<Role>();
		roles.add(roleUtilisateur);
	
		Adresse adresseEtablissement = new Adresse("Tunis", "Beb Bhar", "31 rue Ibn Khouldoun", 2000, null, null);
		Etablissement etablissement = new Etablissement("Auto ecole test", adresseEtablissement, null);

		Moniteur moniteur = new Moniteur("Nasri", "Hichem", "nasri.hichem.pro@gmail.com", "hich123", null,
				"0021652099578", null, roles, etablissement);
		moniteur = (Moniteur) moniteurService.ajouterOuModifierUtilisateur(moniteur);
		agenda.setMoniteur(moniteur);
	}

	@Test
	public void testAjouterOuModifierAgenda() {
		Agenda agendaAjoute = agendaService.creerOuModifierAgenda(agenda);
		assertEquals((long)agendaAjoute.getIdAgenda(), 1L);
		assertEquals(agendaAjoute.getMoniteur().getEmail(), agenda.getMoniteur().getEmail());
	}

}
