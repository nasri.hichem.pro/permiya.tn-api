package tn.permiya.util;

public class PermiyaContantes {
	
	/**
	 * Config Mail
	 */
	public final static String MAIL_TRANSPORT_PROTOCOL="mail.transport.protocol";
	
	public final static String MAIL_SMTP_AUTH="mail.smtp.auth";
	
	public final static String MAIL_SMTP_STARTTLS_ENABLE="mail.smtp.starttls.enable";
	
	public final static String MAIL_DEBUG="mail.debug";
	
	public final static String MAIL_SUBJECT="Bienvenue sur Permiya.tn" ;
		
	public final static String NOM_INJECT="name";
	
	public final static String IDENTIFIANT_INJECT="identifiant";
	
	/**
	 * Constantes utilisés
	 */
	
	public final static String PERMIYA_LOGO_IMAGE_NOM="permiya-logo.png";
	
	public final static String PERMIYA_LOGO_IMAGE_CHEMIN="/templates/images/permiya-logo.PNG";
	
	public final static String HTML_CONTENU_NOM_FICHIER="email-permiya";
	
	public final static String EMAIL_HTML_RESOLVER_TEMPLATE_PREFIX="/templates/";
	
	public final static String EMAIL_HTML_RESOLVER_TEMPLATE_SUFFIX=".html";

}
