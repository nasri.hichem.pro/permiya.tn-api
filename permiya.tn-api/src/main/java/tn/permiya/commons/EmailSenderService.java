package tn.permiya.commons;

import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import tn.permiya.util.PermiyaContantes;

@Component
public class EmailSenderService {

	@Autowired
	public JavaMailSender emailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	public void envoyerEmail(Mail mail) throws MessagingException {

		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());
		helper.addAttachment(PermiyaContantes.PERMIYA_LOGO_IMAGE_NOM,
				new ClassPathResource(PermiyaContantes.PERMIYA_LOGO_IMAGE_CHEMIN));
		Context context = new Context();
		context.setVariables(mail.getProps());

		String html = templateEngine.process(PermiyaContantes.HTML_CONTENU_NOM_FICHIER, context);
		helper.setTo(mail.getMailTo());
		helper.setText(html, true);
		helper.setSubject(mail.getSubject());
		helper.setFrom(mail.getFrom());
		emailSender.send(message);

	}

}
