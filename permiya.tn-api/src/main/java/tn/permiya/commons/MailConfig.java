package tn.permiya.commons;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import tn.permiya.util.PermiyaContantes;

@Configuration
public class MailConfig {
       
	   @Value("${email.host}")
	   private String host ;
	   
	   @Value("${email.username}")
	   private String userName;
	   
	   @Value("${email.password}")
	   private String password;
	   
	   @Value("${email.protocol}")
	   private String protocol;
	   
	   @Value("${email.auth}")
	   private String auth;
	   
	   @Value("${email.enable}")
	   private String enable;
	   
	   @Value("${email.debug}")
	   private String debug ;
	   @Bean
	    public JavaMailSender getJavaMailSender() {
	        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	        mailSender.setHost(host);
	        mailSender.setPort(587);
	 
	        mailSender.setUsername(userName);
	        mailSender.setPassword(password);
	 
	        Properties props = mailSender.getJavaMailProperties();
	        props.put(PermiyaContantes.MAIL_TRANSPORT_PROTOCOL, protocol);
	        props.put(PermiyaContantes.MAIL_SMTP_AUTH, auth);
	        props.put(PermiyaContantes.MAIL_SMTP_STARTTLS_ENABLE, enable);
	        props.put(PermiyaContantes.MAIL_DEBUG, debug);
	 
	        return mailSender;
	    }
	 
}
