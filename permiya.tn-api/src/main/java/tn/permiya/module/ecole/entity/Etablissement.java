package tn.permiya.module.ecole.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import tn.permiya.module.adresse.entite.Adresse;
import tn.permiya.module.gestion.entites.Income;
import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.Moniteur;


/**
*
* @author Permiya.tn Entite Etablissement image Java du TA_ETABLISSEMENT
*
*/
@Entity
@Table(name="TA_ETABLISSEMENT")
@Audited
public class Etablissement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ETABLISSEMENT")
	private Long idEtablissement; 
	
	@Column(name="NOM_ETABLISSEMENT")
	private String nomEtablissement; 
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name="ID_ADRESSE")
	private Adresse adresse ;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_UTILISATEUR")
	@JsonManagedReference
	private Moniteur moniteur ; 
    
	@OneToMany(fetch = FetchType.EAGER,mappedBy="autoEcole")
	@JsonIgnore
	private Set<Eleve>listInscris =new HashSet<Eleve>();
	@OneToMany
	@JoinColumn(name = "ID_INCOMES")
	@JsonIgnore
	private List<Income> incomes;

	public Etablissement() {}
	
	public List<Income> getIncomes() {
		return incomes;
	}

	public void setIncomes(List<Income> incomes) {
		this.incomes = incomes;
	}

	public Etablissement(String nomEtablissement, Adresse adresse,Moniteur moniteur) {
		super();
		this.nomEtablissement = nomEtablissement;
		this.adresse = adresse;
		this.moniteur=moniteur;
	}

	public Long getIdEtablissement() {
		return idEtablissement;
	}

	public String getNomEtablissement() {
		return nomEtablissement;
	}

	public void setNomEtablissement(String nomEtablissement) {
		this.nomEtablissement = nomEtablissement;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Moniteur getMoniteur() {
		return moniteur;
	}

	public void setMoniteur(Moniteur moniteur) {
		this.moniteur = moniteur;
	}

	public void setIdEtablissement(Long idEtablissement) {
		this.idEtablissement = idEtablissement;
	}

	public Set<Eleve> getListInscris() {
		return listInscris;
	}

	public void setListInscris(Set<Eleve> listInscris) {
		this.listInscris = listInscris;
	}
	
	
	
	
	
	

}
