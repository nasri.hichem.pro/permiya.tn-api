package tn.permiya.module.gestion.entites;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.envers.Audited;

import tn.permiya.module.ecole.entity.Etablissement;

@Entity
@Table(name="TA_INCOME")
@Audited
public class Income {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	@Column(name = "VALUE")
	private double value;
    @Temporal(TemporalType.DATE)
	@Column(name = "DATE")
	private Date date;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "COMMENT")
	private String comment;
	@Column(name = "SOURCE")
	private String source;
	@Column(name = "DIRECTION")
	private String direction;
	@ManyToOne(targetEntity = Etablissement.class)
	private Etablissement ecole;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public Income(double value, Date date, String type, String comment, String source) {
		super();
		this.value = value;
		this.date = date;
		this.type = type;
		this.comment = comment;
		this.source = source;
	}
	public Income(Long id, double value, Date date, String type, String comment, String source) {
		super();
		this.id = id;
		this.value = value;
		this.date = date;
		this.type = type;
		this.comment = comment;
		this.source = source;
	}
	public Income(Long id, double value, Date date, String type, String comment, String source, String direction) {
		super();
		this.id = id;
		this.value = value;
		this.date = date;
		this.type = type;
		this.comment = comment;
		this.source = source;
		this.direction = direction;
	}
	public Income(double value, Date date, String type, String comment, String source, String direction) {
		super();
		this.value = value;
		this.date = date;
		this.type = type;
		this.comment = comment;
		this.source = source;
		this.direction = direction;
	}
	public Etablissement getEcole() {
		return ecole;
	}
	public void setEcole(Etablissement ecole) {
		this.ecole = ecole;
	}
	
	
}
