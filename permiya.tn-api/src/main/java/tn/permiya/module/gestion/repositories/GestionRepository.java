package tn.permiya.module.gestion.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import tn.permiya.module.ecole.entity.Etablissement;
import tn.permiya.module.gestion.entites.Income;

@Component
public interface GestionRepository extends JpaRepository<Income, Long> {
	
	
	public List<Income> findByDirectionAndEcole(String dir,Etablissement ecole);
	public List<Income> findByDateAndDirectionAndEcole(Date date,String dirction,Etablissement ecole);
	public List<Income> findByDateBetweenAndDirectionAndEcole(Date date,Date date2,String dirction,Etablissement ecole);

}
