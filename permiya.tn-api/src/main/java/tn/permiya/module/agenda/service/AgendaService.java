package tn.permiya.module.agenda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.agenda.repository.AgendaRepository;

@Component
public class AgendaService {

	@Autowired
	private AgendaRepository agendaRepository;

	public Agenda creerOuModifierAgenda(Agenda agenda) {
		return agendaRepository.save(agenda);
	}

}
