package tn.permiya.module.agenda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import tn.permiya.module.agenda.entite.RendezVous;

@Component
public interface RendezVousRepository extends JpaRepository<RendezVous, Long> {

}
