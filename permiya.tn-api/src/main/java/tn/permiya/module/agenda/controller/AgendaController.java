package tn.permiya.module.agenda.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.agenda.service.AgendaService;
import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.service.MoniteurService;


@RestController
@RequestMapping(path = "/api/agendas")
public class AgendaController {

	@Autowired
	private AgendaService agendaService;
	
	@Autowired
	private MoniteurService moniteurService;

	@PreAuthorize("hasRole('MONITEUR')")
	@ApiOperation(value="Add Agenda", response=Agenda.class)
	@PostMapping(path = "/ajouter", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Agenda> ajouter(@RequestBody Agenda agenda, Principal principal) {
		String email =principal.getName(); 
		Moniteur moniteur=(Moniteur) moniteurService.getUtilisateurByEmail(email).get();
		agenda.setMoniteur(moniteur);
		agenda = agendaService.creerOuModifierAgenda(agenda);
		return ResponseEntity.ok(agenda);
	}

}
