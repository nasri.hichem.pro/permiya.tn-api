package tn.permiya.module.agenda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import tn.permiya.module.agenda.entite.Agenda;

@Component
public interface AgendaRepository extends JpaRepository<Agenda, Long> {

}
