package tn.permiya.module.agenda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.agenda.entite.RendezVous;
import tn.permiya.module.agenda.repository.RendezVousRepository;

@Component
public class RendezVousService {

	@Autowired
	private RendezVousRepository rendezVousRepository;

	@Autowired
	private AgendaService agendaService;
    
	public RendezVous creerUnRDV(Agenda agenda, RendezVous rendezVous) {

		rendezVous = rendezVousRepository.save(rendezVous);
		agenda.getListRendezVous().add(rendezVous);
		agendaService.creerOuModifierAgenda(agenda);
		return rendezVous;

	}

}
