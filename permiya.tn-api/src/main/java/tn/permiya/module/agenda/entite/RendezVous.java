package tn.permiya.module.agenda.entite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

import tn.permiya.module.ecole.entity.Etablissement;
import tn.permiya.module.utilisateur.entite.Eleve;

@Entity
@Table(name="TA_RENDEZ_VOUS")
@Audited

public class RendezVous {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_RDV")
	private Long idRendezVous;
	
	@Column(name="DATE_HEURE_RDV")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateHeureRdv;
	
	@Column(name="HEURE_DEBUT")
	private String heureDebut ;
	
	@Column(name="HEURE_FIN")
	private String heureFin ;
	
	@Column(name="EST_PASSE")
	private boolean estPasse;
	
	@Column(name="NOMBRE_HEURE")
	private int nombreHeure ;
	
	@Column(name="COMMENTAIRE_LECON")
	private String commentaireLecon;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_UTILISATEUR", nullable=false )
	@JsonBackReference
    private Eleve eleve;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_ETABLISSEMENT")
	private Etablissement etablissement;

	public Date getDateHeureRdv() {
		return dateHeureRdv;
	}

	public void setDateHeureRdv(Date dateHeureRdv) {
		this.dateHeureRdv = dateHeureRdv;
	}

	public int getNombreHeure() {
		return nombreHeure;
	}

	public void setNombreHeure(int nombreHeure) {
		this.nombreHeure = nombreHeure;
	}

	public Long getIdRendezVous() {
		return idRendezVous;
	}

	public String getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}

	public String getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}

	public boolean isEstPasse() {
		return estPasse;
	}

	public void setEstPasse(boolean estPasse) {
		this.estPasse = estPasse;
	}

	public Etablissement getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}

	public Eleve getEleve() {
		return eleve;
	}

	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}

	public String getCommentaireLecon() {
		return commentaireLecon;
	}

	public void setCommentaireLecon(String commentaireLecon) {
		this.commentaireLecon = commentaireLecon;
	}

	


	
	

}
