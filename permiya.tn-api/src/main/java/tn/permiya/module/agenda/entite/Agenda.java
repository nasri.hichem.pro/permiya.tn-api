package tn.permiya.module.agenda.entite;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import io.swagger.annotations.ApiModelProperty;
import tn.permiya.module.utilisateur.entite.Moniteur;

@Entity
@Table(name = "TA_AGENDA")
@Audited

public class Agenda {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_AGENDA")
	@ApiModelProperty(name="ID",
    value="The id of the Agenda",
    example="1")
	private Long idAgenda;
	@ApiModelProperty(name="ID_UTILISATEUR",
            value="The id of the User",
            example="1")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_UTILISATEUR")
	private Moniteur moniteur;

	@OneToMany
	@JoinColumn(name = "ID_RENDEZ_VOUS")
	private List<RendezVous> listRendezVous;

	public Agenda() {
	}

	public Moniteur getMoniteur() {
		return moniteur;
	}

	public void setMoniteur(Moniteur moniteur) {
		this.moniteur = moniteur;
	}

	public List<RendezVous> getListRendezVous() {
		if (listRendezVous == null) {
			listRendezVous = new ArrayList<RendezVous>();
		}
		return listRendezVous;
	}

	public void setListRendezVous(List<RendezVous> listRendezVous) {
		this.listRendezVous = listRendezVous;
	}

	public Long getIdAgenda() {
		return idAgenda;
	}

}
