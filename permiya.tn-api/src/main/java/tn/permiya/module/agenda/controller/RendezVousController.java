package tn.permiya.module.agenda.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.agenda.entite.RendezVous;
import tn.permiya.module.agenda.service.RendezVousService;
import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.service.UtilisateurService;

@RestController
@RequestMapping(path = "/api/rendezvous")
public class RendezVousController {
	
	
	@Autowired
	private RendezVousService rendezVousService;
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	@PreAuthorize("hasRole('MONITEUR')")
	@PostMapping(path = "/ajouter", consumes = "application/json", produces = "application/json")
	public ResponseEntity<RendezVous>ajouter(@RequestBody RendezVous rendezVous,Principal principal){
		
		String email=principal.getName();
		Utilisateur utilisateur=utilisateurService.getUtilisateurByEmail(email).get();
	
		if(utilisateur instanceof Moniteur) {
		Moniteur moniteur=(Moniteur) utilisateur;
		Agenda agenda=moniteur.getAgenda();
		rendezVousService.creerUnRDV(agenda, rendezVous);
		}
		else if (utilisateur instanceof Eleve) {
			// à traiter
		}
		return ResponseEntity.ok(rendezVous);
		
	}

}
