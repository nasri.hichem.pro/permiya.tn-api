package tn.permiya.module.adresse.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tn.permiya.module.utilisateur.entite.Utilisateur;

/**
*
* @author Permiya.tn Entite Adresse image Java du TA_Adresse
*
*/
@Entity
@Table(name = "TA_Adresse")
@Audited

public class Adresse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_ADRESSE")
	private Long idAdresse;
	
	@Column(name="GOUVERNORAT")
	private String gouvernorat;
	@Column(name="VILLE")
	private String ville;
	
	@Column(name = "RUE")
	private String rue;
	
	@Column(name = "CODE_POSTALE")
	private int codePostale;
	
	@Column(name = "Longitude", nullable = true)
	private Double longitude;
	
	@Column(name = "Latitude", nullable = true)
	private Double latitude ;
	
	@OneToOne(mappedBy = "adresse")
	@JsonIgnore
	private Utilisateur user;
	
	public Adresse () {
		
	}
    
	
	public Adresse(String gouvernorat,String ville, String rue, int codePostale, Double longitude, Double latitude) {
		super();
		this.gouvernorat=gouvernorat;
		this.ville = ville;
		this.rue = rue;
		this.codePostale = codePostale;
		this.longitude = longitude;
		this.latitude = latitude;
	}



	public Long getIdAdresse() {
		return idAdresse;
	}

  
	public String getRue() {
		return rue;
	}


	public void setRue(String rue) {
		this.rue = rue;
	}


	public int getCodePostale() {
		return codePostale;
	}


	public void setCodePostale(int codePostale) {
		this.codePostale = codePostale;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public String getGouvernorat() {
		return gouvernorat;
	}


	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}


	public String getVille() {
		return ville;
	}


	public void setVille(String ville) {
		this.ville = ville;
	}


	public Utilisateur getUser() {
		return user;
	}


	public void setUser(Utilisateur user) {
		this.user = user;
	}

	

}
