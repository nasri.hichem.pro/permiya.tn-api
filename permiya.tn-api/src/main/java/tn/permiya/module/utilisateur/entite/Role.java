package tn.permiya.module.utilisateur.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "TA_ROLE")
@Audited

public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id ;
	
	
	@Enumerated(EnumType.STRING)
	@Column(length = 20 ,name="NOM_ROLE")
	private NomRole nomRole;
	
	public Role() {
		
	}
	
	public Role(NomRole nomRole) {
		this.nomRole=nomRole;
	}

	public Integer getId() {
		return id;
	}

	public NomRole getNomRole() {
		return nomRole;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNomRole(NomRole nomRole) {
		this.nomRole = nomRole;
	}
	
	
}
