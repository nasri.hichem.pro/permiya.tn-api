package tn.permiya.module.utilisateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.permiya.module.utilisateur.entite.Moniteur;

public interface MoniteurRepositroy extends JpaRepository<Moniteur, Long> {

}
