package tn.permiya.module.utilisateur.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.service.MoniteurService;

@RestController
@RequestMapping(path = "/api/moniteurs")
public class MoniteurController implements UtilisateurController<Moniteur> {

	@Qualifier("moniteurService")
	@Autowired
	private MoniteurService moniteurService;

	@PreAuthorize("hasRole('ELEVE')")
	@Override
	@GetMapping(path = "/liste", produces = "application/json")
	public ResponseEntity<List<Moniteur>> lister() {
		List<Moniteur> listMoniteurs = moniteurService.getListeMoniteurs();
		return ResponseEntity.ok(listMoniteurs);
	}

	@Override
	public ResponseEntity<Moniteur> ajouter(Moniteur t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Moniteur> modifier(Moniteur t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Moniteur>> supprimer(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Moniteur> getUtilisateurById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
