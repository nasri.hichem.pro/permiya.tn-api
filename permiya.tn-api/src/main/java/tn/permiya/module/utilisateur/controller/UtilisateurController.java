package tn.permiya.module.utilisateur.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Permiya.tn [Hichem Nasri] Genrique Utilisateur Controller
 *
 */

@RestController
public interface UtilisateurController<T> {

	public ResponseEntity<List<T>> lister();

	public ResponseEntity<T> ajouter(@RequestBody T t);

	public ResponseEntity<T> modifier(@RequestBody T t);

	public ResponseEntity<List<T>> supprimer(@PathVariable Long id);
	
	public ResponseEntity<T> getUtilisateurById(@PathVariable Long id);

}
