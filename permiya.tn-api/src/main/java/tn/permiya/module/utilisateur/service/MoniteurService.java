package tn.permiya.module.utilisateur.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.repository.MoniteurRepositroy;

@Component
@Qualifier("moniteurService")
public class MoniteurService extends UtilisateurService {
	
	@Autowired
	private MoniteurRepositroy moniteurRepositroy;
	public List<Moniteur>getListeMoniteurs(){
		return moniteurRepositroy.findAll();
	}

}
