package tn.permiya.module.utilisateur.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.entite.UtilisateurDetailsImpl;
import tn.permiya.module.utilisateur.repository.UtilisateurRepository;

@Service
public class UtilisateurDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurRepository.getUtilisateurByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvée par: " + email));

		return UtilisateurDetailsImpl.build(utilisateur);
	}
}
	