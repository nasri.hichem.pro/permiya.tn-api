package tn.permiya.module.utilisateur.entite;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;

import tn.permiya.module.agenda.entite.Agenda;
import tn.permiya.module.ecole.entity.Etablissement;

@Entity
@Table(name="TA_MONITEUR")
@DiscriminatorValue("M")
@Audited

public class Moniteur extends Utilisateur {	
	
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ID_AGENDA")
	private Agenda agenda ;
	
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name="ID_ETABLISSEMENT")
	@JsonBackReference
	private Etablissement etablissement ;
	
	public Moniteur() {}

	public Moniteur(String nom, String prenom, String email, String motPasse, String numeroTelephone,
			String numeroMobile,byte[] photoProfile, Set<Role> roles, Etablissement etablissement) {
		super(nom, prenom, email, motPasse, numeroTelephone,numeroMobile, photoProfile, roles);
		this.etablissement = etablissement;
	}

	public Etablissement getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}

	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	
	
	
	
	
	

}
