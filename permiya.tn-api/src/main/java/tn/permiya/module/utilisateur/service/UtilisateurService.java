package tn.permiya.module.utilisateur.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import tn.permiya.commons.EmailSenderService;
import tn.permiya.commons.Mail;
import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.repository.UtilisateurRepository;
import tn.permiya.util.PermiyaContantes;

@Component
@Qualifier("utilisateurService")
@Primary
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	public EmailSenderService emailSenderService ;
	
	@Value("${email.username}")
	private String from ; 


	public Optional<Utilisateur> getUtilisateurById(Long idUtilisateur) {
		return utilisateurRepository.findById(idUtilisateur);
	}

	public Utilisateur ajouterOuModifierUtilisateur(Utilisateur utilisateur) {
		if (utilisateur.getIdUtilisateur() == 0) {
			utilisateur.setMotPasse(encoder.encode(utilisateur.getMotPasse()));
		}
		if (utilisateur instanceof Eleve) {
			((Eleve) utilisateur).setDateInscription(new Date());
		}
		utilisateur = utilisateurRepository.save(utilisateur);
		if (utilisateur.getIdUtilisateur() != 0) {
		   Mail mail =prepareMailData(utilisateur);
//		   try {
//		emailSenderService.envoyerEmail(mail);
//		} catch (MessagingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		}
		return utilisateur;
	}

	public void supprimerUtilisateur(Utilisateur utilisateur) {
		utilisateurRepository.delete(utilisateur);
	}

	public Iterable<Utilisateur> listUtilisateurs() {
		return utilisateurRepository.findAll();
	}

	public Optional<Utilisateur> getUtilisateurByEmail(String email) {
		return this.utilisateurRepository.getUtilisateurByEmail(email);
	}

	public Boolean existsByEmail(String email) {
		return utilisateurRepository.existsByEmail(email);
	}
	
	private Mail prepareMailData(Utilisateur utilisateur) {
		Mail mail =new Mail();
		mail.setFrom(from);
		mail.setMailTo(utilisateur.getEmail());
		mail.setSubject(PermiyaContantes.MAIL_SUBJECT);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put(PermiyaContantes.NOM_INJECT, utilisateur.getPrenom());
        model.put(PermiyaContantes.IDENTIFIANT_INJECT,utilisateur.getEmail());
        mail.setProps(model);
        return mail;
		
	}

}
