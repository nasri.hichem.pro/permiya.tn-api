package tn.permiya.module.utilisateur.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.Moniteur;
import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.entite.UtilisateurDetailsImpl;
import tn.permiya.module.utilisateur.repository.RoleRepository;
import tn.permiya.module.utilisateur.service.UtilisateurService;
import tn.permiya.security.filter.JwtUtils;

@RestController
@RequestMapping(path = "/api/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UtilisateurService utlisateurService;
	

	@Autowired
	private RoleRepository roleRepositroy;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> login(@RequestBody Utilisateur utilisateur) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(utilisateur.getEmail(), utilisateur.getMotPasse()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UtilisateurDetailsImpl utilisateurDetails = (UtilisateurDetailsImpl) authentication.getPrincipal();
		List<String> roles = utilisateurDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
        if(roles.contains("ROLE_ELEVE")) {
        	Eleve eleve=(Eleve) utlisateurService.getUtilisateurById(utilisateurDetails.getId()).get();
        	eleve.setToken(jwt);
        	return ResponseEntity.ok(eleve);
        }
        else if (roles.contains("ROLE_MONITEUR")) {
        	Moniteur moniteur=(Moniteur) utlisateurService.getUtilisateurById(utilisateurDetails.getId()).get();
        	moniteur.setToken(jwt);
        	return ResponseEntity.ok(moniteur);
        }
		return null;

	}

	@PostMapping(path = "/inscription-eleve", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> registerEleve(@Valid @RequestBody Eleve eleve) {

		if (utlisateurService.existsByEmail(eleve.getEmail())) {
			return ResponseEntity.badRequest().body(new String("Utilisateur deja existe"));
		}

		Set<Role> roles = new HashSet<>();
		Role role = null;
        role=roleRepositroy.getRoleByNomRole(NomRole.ROLE_ELEVE);
        if(role==null) {
        	role = new Role(NomRole.ROLE_ELEVE);
    		roleRepositroy.save(role);
        }
		roles.add(role);
		eleve.setRoles(roles);
		eleve.setDateInscription(new Date());
		eleve=(Eleve) utlisateurService.ajouterOuModifierUtilisateur(eleve);
		return ResponseEntity.ok(eleve);
	}
	
	@PostMapping(path = "/inscription-moniteur", consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> registerMoniteur(@Valid @RequestBody Moniteur moniteur) {

		if (utlisateurService.existsByEmail(moniteur.getEmail())) {
			return ResponseEntity.badRequest().body(new String("Utilisateur deja existe"));
		}

		Set<Role> roles = new HashSet<>();
		Role role = null;
        role=roleRepositroy.getRoleByNomRole(NomRole.ROLE_MONITEUR);
        if(role==null) {
        	role = new Role(NomRole.ROLE_MONITEUR);
    		roleRepositroy.save(role);
        }
		roles.add(role);
		moniteur.setRoles(roles);
		moniteur=(Moniteur)utlisateurService.ajouterOuModifierUtilisateur(moniteur);
		return ResponseEntity.ok(moniteur);
	}
    
	@GetMapping(path = "/confirmerInscription", produces = "application/json")
	public ResponseEntity<String>confirmerInscription(@RequestParam("email")String email){
		Utilisateur utilisateur=utlisateurService.getUtilisateurByEmail(email).get();
		utlisateurService.ajouterOuModifierUtilisateur(utilisateur);
		return ResponseEntity.ok(new String("Votre confirmation est effectué avec succés"));
	}
}
