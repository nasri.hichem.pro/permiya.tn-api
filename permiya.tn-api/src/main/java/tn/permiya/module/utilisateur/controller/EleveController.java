package tn.permiya.module.utilisateur.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.permiya.module.utilisateur.entite.Eleve;
import tn.permiya.module.utilisateur.entite.Utilisateur;
import tn.permiya.module.utilisateur.service.EleveService;

/**
 * 
 * @author Permiya.tn [Hichem Nasri] Eleve Controller
 *
 */
@RestController
@RequestMapping(path = "/api/eleves")
public class EleveController implements UtilisateurController<Eleve> {

	@Autowired
	@Qualifier("eleveService")
	private EleveService eleveService;

	@Override
	@PreAuthorize("hasRole('MONITEUR')")
	@GetMapping(path = "/liste", produces = "application/json")
	public ResponseEntity<List<Eleve>> lister() {
		List<Eleve> listEleves = eleveService.getListeEleves();
		return ResponseEntity.ok(listEleves);
	}
	
	@PreAuthorize("hasRole('MONITEUR')")
	@GetMapping(path = "/listbyecole/{id}", produces = "application/json")
	public ResponseEntity<List<Eleve>> listByEcole(@PathVariable long id) {
		List<Eleve> listEleves = eleveService.getListeElevesByEcole(id);
		return ResponseEntity.ok(listEleves);
	}
	@Override
	@PreAuthorize("hasRole('MONITEUR')")
	@PostMapping(path = "/ajouter", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Eleve> ajouter(@RequestBody Eleve eleve) {
		Eleve eleveAjoute = (Eleve) eleveService.ajouterOuModifierUtilisateur(eleve);
		return ResponseEntity.ok(eleveAjoute);
	}

	@Override
	@PreAuthorize("hasRole('MONITEUR','ELEVE')")
	@PutMapping(path = "/modifier", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Eleve> modifier(@RequestBody Eleve eleve) {
		Eleve eleveModifie = (Eleve) eleveService.ajouterOuModifierUtilisateur(eleve);
		return ResponseEntity.ok(eleveModifie);
	}

	@Override
	@PreAuthorize("hasRole('MONITEUR')")
	@DeleteMapping(path = "/supprimer/{id}", produces = "application/json")
	public ResponseEntity<List<Eleve>> supprimer(@PathVariable Long id) {
		Optional<Utilisateur>eleveSupprime=eleveService.getUtilisateurById(id);
		eleveService.supprimerUtilisateur(eleveSupprime.get());
		return ResponseEntity.ok(eleveService.getListeEleves());
	}
    
	@PreAuthorize("hasRole('ELEVE')")
	@GetMapping(path = "/get/{id}", produces = "application/json")
	@Override
	public ResponseEntity<Eleve> getUtilisateurById(Long id) {
		Eleve eleve= (Eleve) eleveService.getUtilisateurById(id).get();
		return ResponseEntity.ok(eleve);
	}

}
