package tn.permiya.module.utilisateur.entite;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tn.permiya.module.ecole.entity.Etablissement;

@Entity
@DiscriminatorValue("G")
@Table(name = "TA_GESTIONNEUR")
@Audited

public class Gestionneur extends Utilisateur {
	
    
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnore
	private Etablissement autoEcole;
	
	public Gestionneur() {
		
	}
	
	public Etablissement getAutoEcole() {
		return autoEcole;
	}
	public void setAutoEcole(Etablissement autoEcole) {
		this.autoEcole = autoEcole;
	}
	
	
	
	

}
