package tn.permiya.module.utilisateur.entite;

public enum NomRole {
	ROLE_ADMIN,
	ROLE_GESTIONNEUR,
	ROLE_UTILISATEUR,
	ROLE_MONITEUR,
	ROLE_ELEVE

}
