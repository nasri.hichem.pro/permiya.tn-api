package tn.permiya.module.utilisateur.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import tn.permiya.module.utilisateur.entite.Eleve;

import tn.permiya.module.utilisateur.repository.EleveRepository;

@Component
@Qualifier("eleveService")
public class EleveService extends UtilisateurService {
	
	
	@Autowired
	private EleveRepository eleveRepository;
	
	
	public List<Eleve>getListeEleves(){
		return eleveRepository.findAll();
	}
	public List<Eleve>getListeElevesByEcole(long idEcole){
		return eleveRepository.findByEcole(idEcole);
	}
}
