package tn.permiya.module.utilisateur.entite;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tn.permiya.module.agenda.entite.RendezVous;
import tn.permiya.module.ecole.entity.Etablissement;

@Entity
@DiscriminatorValue("E")
@Table(name = "TA_ELEVE")
@Audited
public class Eleve extends Utilisateur {

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "DATA_Inscription", nullable = false)
	private Date dateInscription;
	
	
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="eleve")
	@JsonIgnore
	private Set<RendezVous>listRendezVous =new HashSet<RendezVous>();
	
    
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonIgnore
	private Etablissement autoEcole;
	
	public Eleve() {
		
	}
	public Eleve(String nom, String prenom, String email, String motPasse, String numeroTelephone,String numeroMobile, byte[] photoProfile,
			Set<Role> roles, Date dateInscription,Set<RendezVous>listRendezVous) {
		super(nom, prenom, email, motPasse, numeroTelephone,numeroMobile, photoProfile, roles);
		this.listRendezVous=listRendezVous;
		this.dateInscription = dateInscription;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public Set<RendezVous> getListRendezVous() {
		if(listRendezVous==null) {
			listRendezVous=new HashSet<RendezVous>();
		}
		return listRendezVous;
	}
	public void setListRendezVous(Set<RendezVous> listRendezVous) {
		this.listRendezVous = listRendezVous;
	}
	public Etablissement getAutoEcole() {
		return autoEcole;
	}
	public void setAutoEcole(Etablissement autoEcole) {
		this.autoEcole = autoEcole;
	}
	
	
	
	

}
