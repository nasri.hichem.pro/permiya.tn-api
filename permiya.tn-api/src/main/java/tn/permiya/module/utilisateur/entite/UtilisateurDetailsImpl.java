package tn.permiya.module.utilisateur.entite;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UtilisateurDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String email;

	@JsonIgnore
	private String motPasse;

	private Collection<? extends GrantedAuthority> authorities;

	public UtilisateurDetailsImpl(Long id,String email, String motPasse,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.email = email;
		this.motPasse = motPasse;
		this.authorities = authorities;
	}

	public static UtilisateurDetailsImpl build(Utilisateur utilisateur) {
		List<GrantedAuthority> authorities = utilisateur.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getNomRole().name()))
				.collect(Collectors.toList());

		return new UtilisateurDetailsImpl(
				utilisateur.getIdUtilisateur(), 
				utilisateur.getEmail(),
				utilisateur.getMotPasse(), 
				authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UtilisateurDetailsImpl user = (UtilisateurDetailsImpl) o;
		return Objects.equals(id, user.id);
	}

	@Override
	public String getPassword() {
		return motPasse;
	}

	@Override
	public String getUsername() {
		return email;
	}
}
