package tn.permiya.module.utilisateur.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import tn.permiya.module.utilisateur.entite.Utilisateur;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
	
	
	public Optional<Utilisateur> getUtilisateurByEmail(@Param("email")String email);
	public Boolean existsByEmail(String email);
	//public void linkUsertoEcole(long idUser, long idEcole);
   
}
