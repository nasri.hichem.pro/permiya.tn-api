package tn.permiya.module.utilisateur.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import tn.permiya.module.utilisateur.entite.Eleve;

@Repository
public interface EleveRepository extends JpaRepository<Eleve, Long> {

	  @Query("select u from Eleve u where u.autoEcole.idEtablissement = ?1")
	  public List<Eleve> findByEcole(long idEcole);

}
