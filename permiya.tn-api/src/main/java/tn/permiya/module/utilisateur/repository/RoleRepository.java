package tn.permiya.module.utilisateur.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.permiya.module.utilisateur.entite.NomRole;
import tn.permiya.module.utilisateur.entite.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	
	
	public Role getRoleByNomRole(NomRole nomRole);
	
	
}
