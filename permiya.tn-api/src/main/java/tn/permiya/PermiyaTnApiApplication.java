package tn.permiya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * @author Permiya.tn [Hichem Nasri]
 * Classe lancement de l'application Permiya.tn
 *
 */
@SpringBootApplication
@ComponentScan({"tn.permiya","tn.permiya.module","tn.permiya.security","tn.permiya.commons"})
@Configuration
public class PermiyaTnApiApplication {
    

	public static void main(final String[] args) {
		SpringApplication.run(PermiyaTnApiApplication.class, args);
	}
	
	
}
